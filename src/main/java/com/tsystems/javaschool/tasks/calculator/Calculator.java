package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {
    static boolean divisionbyZero = false;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null) {
            return null;
        }
        String allowedSymbols = "[0-9-+/*.()]+";
        String onlyOnce = "([.+\\-/*])\\1+";
        final Pattern pattern = Pattern.compile(onlyOnce, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(statement);
        if (!statement.matches(allowedSymbols) ||
                !validParentheses(statement.toCharArray()) ||
                (matcher.find())
        ) {
            return null;
        }

        double number = calculate(toPolishNotation(statement));
        if (divisionbyZero) {
            return null;
        }
        if (number % 1 == 0) {
            return String.valueOf((int) number);
        } else {
            return String.valueOf(number);
        }
    }

    public static boolean validParentheses(char[] data) {
        int parentheses = 0;
        for (char myChar : data) {
            if (myChar == '(') {
                parentheses++;
            } else if (myChar == ')') {
                parentheses--;
            }
            if (parentheses < 0) {
                return false;
            }
        }
        return parentheses == 0;
    }

    /**
     * Converting the statement to the reversed Polish notation
     *
     * @param statement incoming statement
     * @return output line in reversed Polish notation
     */
    private static String toPolishNotation(String statement) {
        StringBuilder sbStack = new StringBuilder(), sbOut = new StringBuilder();
        char cIn, cTmp;

        for (int i = 0; i < statement.length(); i++) {
            cIn = statement.charAt(i);
            if (isOperator(cIn)) {
                while (sbStack.length() > 0) {
                    cTmp = sbStack.substring(sbStack.length() - 1).charAt(0);
                    if (isOperator(cTmp) && (operationPriority(cIn) <= operationPriority(cTmp))) {
                        sbOut.append(" ").append(cTmp).append(" ");
                        sbStack.setLength(sbStack.length() - 1);
                    } else {
                        sbOut.append(" ");
                        break;
                    }
                }
                sbOut.append(" ");
                sbStack.append(cIn);
            } else if ('(' == cIn) {
                sbStack.append(cIn);
            } else if (')' == cIn) {
                cTmp = sbStack.substring(sbStack.length() - 1).charAt(0);
                while ('(' != cTmp) {
                    sbOut.append(" ").append(cTmp);
                    sbStack.setLength(sbStack.length() - 1);
                    cTmp = sbStack.substring(sbStack.length() - 1).charAt(0);
                }
                sbStack.setLength(sbStack.length() - 1);
            } else {
                // append the output line if the symbol is not an operator
                sbOut.append(cIn);
            }
        }

        // Adding the operators to the input line if any
        while (sbStack.length() > 0) {
            sbOut.append(" ").append(sbStack.substring(sbStack.length() - 1));
            sbStack.setLength(sbStack.length() - 1);
        }
        return sbOut.toString();
    }

    /**
     * Checking if the current symbol is an operator
     */
    private static boolean isOperator(char c) {
        return c == '-' || c == '+' || c == '*' || c == '/';
    }

    /**
     * Returns the priority of the operations     *
     *
     * @param operation char
     * @return byte
     */
    private static byte operationPriority(char operation) {
        if (operation == '*' || operation == '/') {
            return 2;
        }
        return 1;

    }

    /**
     * Calculate the reversed Polish notation     *
     *
     * @param statement
     * @return double result
     */
    private static Double calculate(String statement) {
        double leftData, rightData;
        String sTmp;
        Deque<Double> stack = new ArrayDeque<>();
        StringTokenizer st = new StringTokenizer(statement);
        while (st.hasMoreTokens()) {
            sTmp = st.nextToken().trim();
            if (1 == sTmp.length() && isOperator(sTmp.charAt(0))) {
                rightData = stack.pop();
                leftData = stack.pop();
                if (sTmp.charAt(0) == '+') {
                    leftData += rightData;
                } else if (sTmp.charAt(0) == '-') {
                    leftData -= rightData;
                } else if (sTmp.charAt(0) == '/'){
                    if (rightData != 0) {
                        leftData /= rightData;
                    } else {
                        divisionbyZero = true;
                        return 0.0;
                    }                    
                } else if (sTmp.charAt(0) == '*') {
                    leftData *= rightData;
                }                
                stack.push(leftData);
            } else {
                leftData = Double.parseDouble(sTmp);
                stack.push(leftData);
            }
        }
        if (stack.size() > 1) {
            System.out.println("The amount of operators do not correspond with the amount of operands");
        }
        return stack.pop();
    }

}