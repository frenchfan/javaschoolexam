package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {
    private static int rows, columns;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO: Implement the logic here
        if (inputNumbers == null  ||
                inputNumbers.contains(null) ||
                !checkTriangle(inputNumbers.size()) ||
                Collections.max(inputNumbers) == 0) { // no sense to build the pyramid with 0s only
            throw new CannotBuildPyramidException();
        }
        Collections.sort(inputNumbers);
        int[][] pyramid = new int[rows][columns];
        int center = (columns / 2);
        int counter = 1;
        int index = 0;

        for (int i = 0, offset = 0; i < rows; i++, offset++, counter++) {
            int start = center - offset;
            for (int j = 0; j < counter * 2; j += 2, index++) {
                pyramid[i][start + j] = inputNumbers.get(index);
            }
        }
        return pyramid;
    }

    /**
     * Check if the triangle (out of the collection) is possible and sets the actual
     * amount of columns and rows
     * @param size collection's size
     * @return boolean
     */
    private static boolean checkTriangle(long size) {
        if (size < 3) {
            throw new CannotBuildPyramidException();
        }
        int counter = 0;
        rows = 1;
        columns = 1;
        boolean rsl = false;
        while (counter < size) {
            counter = counter + rows;
            rows++;
            columns = columns + 2;
        }
        rows = rows - 1;
        columns = columns - 2;
        if (size == counter) {
            rsl = true;
        }
        return rsl;
    }
}
