package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if ((x == null) || (y == null)) {
            throw new IllegalArgumentException();
        }
        boolean result = true;
        int counter = 0;
        for (Object o : x) {
            if (result) {
                result = false;
                for (int i = counter; i < y.size(); i++) {
                    if (y.get(i).equals(o)) {
                        counter = i;
                        result = true;
                        break;
                    }
                }
            } else break;
        }
        return result;
    }
}
